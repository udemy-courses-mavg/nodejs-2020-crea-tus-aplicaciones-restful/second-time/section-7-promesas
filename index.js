/**
 *  PROMESA. Es un objeto que representa la terminación o el fracaso eventual de una operación asíncrono.
 * Tiene dos estados, el estado inicial que siempre es pendin (pendiente), y tendrá este estado hasta que la promesa se haya resuelto mediante resolve o reject, 
 * es decir el estado final solo podrá tener dos estados Resolve (ha resuleto la promesa y devuelve el dato solicitado) o Reject (no ha podido finalizar la promesa y devuelve un error)
 */

const promesa = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve({id: 1, model: 'Leon', company: 'SEAT'})
    // reject(new Error('Se ha producido un error al leer la base de datos'))
  }, 3000)
})

promesa
  .then(result => console.log(result))
  .catch(err => console.log(err.message))